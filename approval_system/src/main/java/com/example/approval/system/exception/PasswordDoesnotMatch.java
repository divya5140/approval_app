package com.example.approval.system.exception;

public class PasswordDoesnotMatch extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public PasswordDoesnotMatch() {
        super("Password does not match");
    }


	public PasswordDoesnotMatch(String message) {
        super(message);
    }

}
