package com.example.approval.system.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
	
	@Id
	private String employeeId;
	
	private String firstName;
	
	private String lastName;
	
	private String role;
	
	private String managerId;
	
	@Email
	@Column(unique = true)
	private String emailId;
	
	private String password;
	
	@Enumerated(EnumType.STRING)
	private LogStatus logStatus;
	
	private int age;

}
