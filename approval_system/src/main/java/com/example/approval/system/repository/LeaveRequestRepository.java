package com.example.approval.system.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.approval.system.entity.LeaveRequest;

public interface LeaveRequestRepository extends JpaRepository<LeaveRequest, String>{

	Optional<LeaveRequest> findByRequestId(String request_id);

}
