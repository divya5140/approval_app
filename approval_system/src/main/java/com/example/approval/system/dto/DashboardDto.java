package com.example.approval.system.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashboardDto {
	
	private List<ApproverRequestDto> requestedleaves;
	
	private List<ApproverRequestDto> pendingrequests;
	
	

}
