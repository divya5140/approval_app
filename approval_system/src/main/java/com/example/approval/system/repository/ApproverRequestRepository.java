package com.example.approval.system.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.approval.system.entity.ApproverRequest;
import com.example.approval.system.entity.Employee;

public interface ApproverRequestRepository extends JpaRepository<ApproverRequest, Integer> {

	ApproverRequest  findBySourcerequestId(String sourcerequestId);

	
	List<ApproverRequest> findByApproverId(String employeeid);

	List<ApproverRequest> findByEmployee(Employee employee);

	
	
}
