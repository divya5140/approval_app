package com.example.approval.system.exception;

public class LoginRequiredException extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoginRequiredException() {
        super( "Login Required ");
    }

    public LoginRequiredException(String message) {
        super(message );
    }
}
