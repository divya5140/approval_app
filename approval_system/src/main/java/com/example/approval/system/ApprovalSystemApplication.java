package com.example.approval.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApprovalSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApprovalSystemApplication.class, args);
	}

}
