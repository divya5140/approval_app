package com.example.approval.system.kafka.sender;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.approval.system.dto.ApproverUpdateDto;
import com.example.approval.system.kafka.serializer.ApproverUpdateSerializer;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LeaveDataSender {
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(ApproverUpdateDto approverUpdateDto) {
		
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ApproverUpdateSerializer.class);

		
		Producer<String, ApproverUpdateDto> kafkaProducer = new KafkaProducer<>(properties, new StringSerializer(),
				new ApproverUpdateSerializer());	
		
		log.info("Message Sent: "+approverUpdateDto);
		System.out.println();
		ProducerRecord<String, ApproverUpdateDto> record = new ProducerRecord<String, ApproverUpdateDto>("approverupdate", approverUpdateDto);
		log.info(""+record);
		System.out.println(kafkaProducer.send(record));
		
 
		
	}
}
