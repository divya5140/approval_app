package com.example.approval.system.exception;

public class NotAManager extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotAManager() {
        super( "Not a manager to take the action ");
    }

    public NotAManager(String message) {
        super(message );
    }

}
