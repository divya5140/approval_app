package com.example.approval.system.entity;

public enum RequestStatus {

	APPROVED,REJECTED,REFERREDBACK,SUBMITTED,RESUBMITTED,PENDING,CANCELLED
	
}
