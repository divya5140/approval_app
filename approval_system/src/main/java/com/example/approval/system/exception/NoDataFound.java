package com.example.approval.system.exception;

public class NoDataFound extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoDataFound() {
        super( "No data found ");
    }

    public NoDataFound(String message) {
        super(message );
    }
}
