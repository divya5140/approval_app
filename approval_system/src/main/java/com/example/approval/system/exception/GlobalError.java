package com.example.approval.system.exception;

public class GlobalError {

	public static final String NOT_FOUND ="404";

	public static final  String CONFLICT = "409";

	public static final String BADREQUEST = "400";

}