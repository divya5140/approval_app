package com.example.approval.system.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {

		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse response = new ErrorResponse("400", details);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(EmployeeDoesnotExist.class)
	public ResponseEntity<Object> handleCustomerNotFoundException(EmployeeDoesnotExist ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse("404", errorMessage), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(NotAManager.class)
	public ResponseEntity<Object> handleNotAManager(NotAManager ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse("400", errorMessage), HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(PasswordDoesnotMatch.class)
	public ResponseEntity<Object> handlePasswordDoesnotMatch(PasswordDoesnotMatch ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse("400", errorMessage), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NoDataFound.class)
	public ResponseEntity<Object> handleNoDataFound(NoDataFound ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse("400", errorMessage), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(LeaveRequestAlreadyCancelledException.class)
	public ResponseEntity<Object> handleLeaveRequestAlreadyCancelledException(LeaveRequestAlreadyCancelledException ex,
			WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse("400", errorMessage), HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(AlreadyLoggedin.class)
	public ResponseEntity<Object> handleAlreadyLoggedin(AlreadyLoggedin ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse("400", errorMessage), HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler(LoginRequiredException.class)
	public ResponseEntity<Object> handleLoginRequired(LoginRequiredException ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse("400", errorMessage), HttpStatus.BAD_REQUEST);
	}
	
}
