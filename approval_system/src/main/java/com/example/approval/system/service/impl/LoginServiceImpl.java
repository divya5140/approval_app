package com.example.approval.system.service.impl;

import org.springframework.stereotype.Service;

import com.example.approval.system.dto.LoginDto;
import com.example.approval.system.dto.ResponseDto;
import com.example.approval.system.entity.Employee;
import com.example.approval.system.entity.LogStatus;
import com.example.approval.system.exception.AlreadyLoggedin;
import com.example.approval.system.exception.EmployeeDoesnotExist;
import com.example.approval.system.exception.PasswordDoesnotMatch;
import com.example.approval.system.repository.EmployeeRepository;
import com.example.approval.system.service.LoginService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {
	
	private final EmployeeRepository employeeRepository;
	
	public ResponseDto login(LoginDto loginDto)
	{
		Employee employee=employeeRepository.findByEmployeeId(loginDto.getEmployeeId()).orElseThrow(()->
		{
		
			throw new EmployeeDoesnotExist("Employee with Id:- " +loginDto.getEmployeeId()+ " does not exist");
		});
		if(employee.getLogStatus().equals(LogStatus.LOGIN))
		{
			throw new AlreadyLoggedin("Already logged In");
		}
		if(!employee.getEmailId().equals(loginDto.getPassword()))
		{
			throw new PasswordDoesnotMatch("Password does not match");
		}
		employee.setLogStatus(LogStatus.LOGIN);
		employeeRepository.save(employee);
		log.info("Employee with Id " +loginDto.getEmployeeId()+" Logged In successfully");
		return new ResponseDto("Employee with Id " +loginDto.getEmployeeId()+" Logged In successfully");
	}

}
