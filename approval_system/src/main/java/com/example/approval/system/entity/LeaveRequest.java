package com.example.approval.system.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeaveRequest {

	@Id
	private String requestId;

	@ManyToOne
	private Employee employee;
	
	private String managerId;

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startDateTime;

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endDateTime;

	@Enumerated(EnumType.STRING)
	private RequestType requestType;

	private String Reason;

	@Enumerated(EnumType.STRING)
	@Column(length = 50)
	private RequestStatus requestStatus;

	private String remarks;
	
	@CreationTimestamp
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime creationDateTime;

	@UpdateTimestamp
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updationDateTime;

}
