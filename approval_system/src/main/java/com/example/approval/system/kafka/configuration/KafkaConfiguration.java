package com.example.approval.system.kafka.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import com.example.approval.system.dto.ManagerRequestDto;
import com.example.approval.system.kafka.deserializer.KafkaDeserializer;

@Configuration
public class KafkaConfiguration {
	
	@Bean
	ConsumerFactory<String, ManagerRequestDto> consumerFactory() {
		
		Map<String, Object> properties = new HashMap<>();
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaDeserializer.class);
		properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		
		return new DefaultKafkaConsumerFactory<>(properties, new StringDeserializer(), new KafkaDeserializer());
	}
	
	@Bean
	ConcurrentKafkaListenerContainerFactory<String, ManagerRequestDto> concurrentKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, ManagerRequestDto> containerFactory = new ConcurrentKafkaListenerContainerFactory<>();
		containerFactory.setConsumerFactory(consumerFactory());
		return containerFactory;
	}


}
