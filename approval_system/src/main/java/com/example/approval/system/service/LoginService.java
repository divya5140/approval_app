package com.example.approval.system.service;

import com.example.approval.system.dto.LoginDto;
import com.example.approval.system.dto.ResponseDto;

public interface LoginService {
	
	ResponseDto login(LoginDto loginDto);

}
