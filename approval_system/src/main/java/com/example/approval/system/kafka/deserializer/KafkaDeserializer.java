package com.example.approval.system.kafka.deserializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.approval.system.dto.ManagerRequestDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaDeserializer implements Deserializer<ManagerRequestDto>{

	@Override
	public ManagerRequestDto deserialize(String topic, byte[] data) {
		
		try
		{
			ObjectMapper mapper=new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			if(Objects.isNull(data))
			{
				log.error("Null received at deserializer");
				return null;
			}
			return mapper.readValue(data, ManagerRequestDto.class);
		}
		catch (IOException e) {
			throw new SerializationException("Error when deserilaization");
		}
		
	}


}
