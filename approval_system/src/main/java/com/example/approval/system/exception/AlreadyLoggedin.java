package com.example.approval.system.exception;

public class AlreadyLoggedin extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AlreadyLoggedin() {
        super( "Already LoggedIn ");
    }

    public AlreadyLoggedin(String message) {
        super(message );
    }
}

