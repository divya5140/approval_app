package com.example.approval.system.exception;

public class EmployeeDoesnotExist extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmployeeDoesnotExist() {
        super( "Employee doesnot exist ");
    }

    public EmployeeDoesnotExist(String message) {
        super(message );
    }
}
