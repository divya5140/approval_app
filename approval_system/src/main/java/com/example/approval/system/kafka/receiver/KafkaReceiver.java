package com.example.approval.system.kafka.receiver;

import java.util.Optional;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.approval.system.dto.ManagerRequestDto;
import com.example.approval.system.entity.ApproverRequest;
import com.example.approval.system.entity.Employee;
import com.example.approval.system.repository.ApproverRequestRepository;
import com.example.approval.system.repository.EmployeeRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaReceiver {

	private final ApproverRequestRepository approverRequestRepository;

	private final EmployeeRepository employeeRepository;

	@KafkaListener(topics = "leaverequests", groupId = "group-id", containerFactory = "concurrentKafkaListenerContainerFactory")
	public void consume(ManagerRequestDto managerRequestDto) {

		ApproverRequest approverRequest = new ApproverRequest();

		Optional<Employee> employee = employeeRepository.findByEmployeeId(managerRequestDto.getEmployeeId());

		approverRequest.setRequestsystemid("LMS");
		approverRequest.setEmployee(employee.get());
		approverRequest.setApproverId(managerRequestDto.getApproverId());
		approverRequest.setSourcerequestId(managerRequestDto.getRequestId());
		
		
		approverRequest.setStatus(managerRequestDto.getRequestStatus());
		approverRequest.setDescription("StartDate:"+managerRequestDto.getStartDateTime().toLocalDate()+" "+managerRequestDto.getStartDateTime().toLocalTime() +" ,EndDate:"+managerRequestDto.getEndDateTime().toLocalDate()+" "+managerRequestDto.getEndDateTime().toLocalTime()+" ,RequestTpe:"+managerRequestDto.getRequestType()+" ,Reason:"+managerRequestDto.getReason());
		
		approverRequestRepository.save(approverRequest);
		log.info(managerRequestDto.toString());
	}

}
