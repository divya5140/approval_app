package com.example.approval.system.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApproverDto {

	private String approverId;

	private String requestId;
	
	private String status;

	private String remarks;
}
