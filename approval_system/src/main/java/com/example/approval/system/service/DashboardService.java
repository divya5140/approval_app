package com.example.approval.system.service;

import com.example.approval.system.dto.DashboardDto;

public interface DashboardService {

	DashboardDto dashboardview(String employeeid);
	
	
}
