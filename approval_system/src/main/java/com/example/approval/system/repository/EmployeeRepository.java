package com.example.approval.system.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.approval.system.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String>{

	Optional<Employee> findByEmployeeId(String employeeId);

	

	

}
