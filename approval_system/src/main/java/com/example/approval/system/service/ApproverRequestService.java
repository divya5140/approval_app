package com.example.approval.system.service;

import com.example.approval.system.dto.ApproverDto;
import com.example.approval.system.dto.ResponseDto;

public interface ApproverRequestService {
	
	ResponseDto approverrequests(ApproverDto approverDto);

}
