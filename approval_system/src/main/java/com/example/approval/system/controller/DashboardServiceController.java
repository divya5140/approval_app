package com.example.approval.system.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.approval.system.dto.DashboardDto;
import com.example.approval.system.service.DashboardService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class DashboardServiceController {

	private final DashboardService dashboardService;

	@GetMapping("/viewRequests")
	public ResponseEntity<DashboardDto> viewDashboard(@RequestParam String employeeId) {
		return new ResponseEntity<>(dashboardService.dashboardview(employeeId), HttpStatus.OK);
	}
}
