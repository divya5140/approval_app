package com.example.approval.system.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.approval.system.dto.ApproverDto;
import com.example.approval.system.dto.LeaveDto;
import com.example.approval.system.dto.LeaveRequestDto;
import com.example.approval.system.dto.ResponseDto;
import com.example.approval.system.service.LeaveRequestService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class LeaveRequestController {
	
	private final LeaveRequestService leaveRequestService;
	
	@PostMapping("/addRequest")
	public ResponseEntity<ResponseDto> login(@RequestBody LeaveRequestDto leaveRequestDto)
	{
		return new ResponseEntity<ResponseDto>(leaveRequestService.leaverequest(leaveRequestDto), HttpStatus.OK);
	}
	
	
	@GetMapping("/viewRequest")
	public ResponseEntity<LeaveDto> getData(@RequestParam String requestId)
	{
		return new ResponseEntity<LeaveDto>(leaveRequestService.getRequest(requestId), HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteRequest")
	public ResponseEntity<ResponseDto> deleteRequest(@RequestParam String requestId)
	{
		return new ResponseEntity<ResponseDto>(leaveRequestService.deleterequest(requestId), HttpStatus.OK);
	}
	
	@PostMapping("/takeaction")
	public ResponseEntity<ResponseDto> takeaction(@RequestBody ApproverDto approverDto) {
		return new ResponseEntity<>(leaveRequestService.approverupdate(approverDto), HttpStatus.OK);
	}

}
