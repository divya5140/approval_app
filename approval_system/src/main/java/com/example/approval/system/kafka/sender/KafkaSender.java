package com.example.approval.system.kafka.sender;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.approval.system.dto.ManagerRequestDto;
import com.example.approval.system.kafka.serializer.KafkaSerializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KafkaSender {

	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(ManagerRequestDto managerRequestDto) {
		
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaSerializer.class);

		
		Producer<String, ManagerRequestDto> kafkaProducer = new KafkaProducer<>(properties, new StringSerializer(),
				new KafkaSerializer());	
		
		log.info("Message Sent: "+managerRequestDto);
		System.out.println();
		ProducerRecord<String, ManagerRequestDto> record = new ProducerRecord<String, ManagerRequestDto>("leaverequests", managerRequestDto);
		log.info(""+record);
		System.out.println(kafkaProducer.send(record));
		
 
		
	}

}
