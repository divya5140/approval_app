package com.example.approval.system.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.approval.system.dto.ApproverDto;
import com.example.approval.system.dto.ResponseDto;
import com.example.approval.system.service.ApproverRequestService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class ApproverRequestController {

	private final ApproverRequestService approverRequestService;
	
	@PostMapping("/take-action")
	public ResponseEntity<ResponseDto> actionTaken(@RequestBody ApproverDto approverDto) {
		return new ResponseEntity<>(approverRequestService.approverrequests(approverDto), HttpStatus.OK);
	}
}
