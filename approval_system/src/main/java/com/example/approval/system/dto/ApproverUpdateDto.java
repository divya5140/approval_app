package com.example.approval.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApproverUpdateDto {


	
	private String sourcerequestId;

	private String status;
	
	private String remarks;
}
