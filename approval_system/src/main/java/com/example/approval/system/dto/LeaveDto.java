package com.example.approval.system.dto;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LeaveDto {
	

	private String employeeId;
	
	private String managerId;

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startDateTime;

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endDateTime;

	
	private String requestType;

	private String reason;

	
	private String requestStatus;

	
	@CreationTimestamp
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime creationDateTime;

	@CreationTimestamp
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updationDateTime;
}
