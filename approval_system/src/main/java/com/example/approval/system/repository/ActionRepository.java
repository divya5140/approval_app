package com.example.approval.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.approval.system.entity.Action;

public interface ActionRepository extends JpaRepository<Action, Integer>{

	Action findByRequestId(String requestId);
}
