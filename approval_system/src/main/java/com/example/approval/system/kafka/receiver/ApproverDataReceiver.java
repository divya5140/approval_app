package com.example.approval.system.kafka.receiver;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.approval.system.dto.ApproverUpdateDto;
import com.example.approval.system.entity.ApproverRequest;
import com.example.approval.system.repository.ApproverRequestRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ApproverDataReceiver {
	private final ApproverRequestRepository approverRequestRepository;

	@KafkaListener(topics = "approverupdate", groupId = "groups-ids", containerFactory = "concurrentListenerContainerFactory")
	public void consume(ApproverUpdateDto approverUpdateDto) {

		ApproverRequest approverRequest = approverRequestRepository
				.findBySourcerequestId(approverUpdateDto.getSourcerequestId());

	
		approverRequest.setStatus(approverUpdateDto.getStatus());
		approverRequest.setRemarks(approverUpdateDto.getRemarks());
		approverRequestRepository.save(approverRequest);

		log.info(approverUpdateDto.toString());

	}
}
