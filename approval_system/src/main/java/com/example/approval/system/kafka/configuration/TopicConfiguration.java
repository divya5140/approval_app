package com.example.approval.system.kafka.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class TopicConfiguration {

	@Bean
	NewTopic approverupdate() {
		return TopicBuilder.name("approverupdate")
				.partitions(4).build();
	}
	
	@Bean
	NewTopic leaverequests() {
		return TopicBuilder.name("leaverequests")
				.partitions(4).build();
	}
	
	@Bean
	NewTopic leaveupdate() {
		return TopicBuilder.name("leaveupdate")
				.partitions(4).build();
	}

}
