package com.example.approval.system.kafka.serializer;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import com.example.approval.system.dto.LeaveUpdateDto;
import com.example.approval.system.dto.ManagerRequestDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LeaveUpdateSerializer implements Serializer<LeaveUpdateDto> {

		@Override
		public byte[] serialize(String topic,LeaveUpdateDto  data) {

			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.registerModule(new JavaTimeModule());
				if (Objects.isNull(data)) {
					log.error("Null value received at the serializer");
					return null;
				}
				return mapper.writeValueAsBytes(data);
			} catch (JsonProcessingException e) {
				throw new SerializationException("Error occured while serializing the data");
			}

		}
}
