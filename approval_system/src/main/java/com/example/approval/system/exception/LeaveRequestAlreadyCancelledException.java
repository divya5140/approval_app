package com.example.approval.system.exception;

public class LeaveRequestAlreadyCancelledException extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LeaveRequestAlreadyCancelledException() {
        super( "Leave request already cancelled ");
    }

    public LeaveRequestAlreadyCancelledException(String message) {
        super(message );
    }
}