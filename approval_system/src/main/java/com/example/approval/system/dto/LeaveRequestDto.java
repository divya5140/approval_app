package com.example.approval.system.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LeaveRequestDto {

	private String employeeId;

	private LocalDateTime startDateTime;

	private LocalDateTime endDateTime;

	private String requestType;

	private String Reason;

}
