package com.example.approval.system.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.example.approval.system.dto.ApproverDto;
import com.example.approval.system.dto.ApproverUpdateDto;
import com.example.approval.system.dto.LeaveDto;
import com.example.approval.system.dto.LeaveRequestDto;
import com.example.approval.system.dto.ManagerRequestDto;
import com.example.approval.system.dto.ResponseDto;
import com.example.approval.system.entity.Action;
import com.example.approval.system.entity.ApproverRequest;
import com.example.approval.system.entity.Employee;
import com.example.approval.system.entity.LeaveRequest;
import com.example.approval.system.entity.LogStatus;
import com.example.approval.system.entity.RequestStatus;
import com.example.approval.system.entity.RequestType;
import com.example.approval.system.exception.EmployeeDoesnotExist;
import com.example.approval.system.exception.LeaveRequestAlreadyCancelledException;
import com.example.approval.system.exception.LoginRequiredException;
import com.example.approval.system.exception.NoDataFound;
import com.example.approval.system.exception.NotAManager;
import com.example.approval.system.kafka.sender.KafkaSender;
import com.example.approval.system.kafka.sender.LeaveDataSender;
import com.example.approval.system.repository.ActionRepository;
import com.example.approval.system.repository.ApproverRequestRepository;
import com.example.approval.system.repository.EmployeeRepository;
import com.example.approval.system.repository.LeaveRequestRepository;
import com.example.approval.system.service.LeaveRequestService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class LeaveRequestServiceImpl implements LeaveRequestService {

	private final LeaveRequestRepository leaveRequestRepository;

	private final ApproverRequestRepository approverRequestRepository;

	private final KafkaSender kafkaSender;

	private final LeaveDataSender leavedatasender;

	private final ActionRepository actionRepository;

	private final EmployeeRepository employeeRepository;

	public ResponseDto leaverequest(LeaveRequestDto leaveRequestDto) {
		Employee employee = employeeRepository.findByEmployeeId(leaveRequestDto.getEmployeeId()).orElseThrow(() -> {

			log.error("Employee with Id:-" + leaveRequestDto.getEmployeeId() + " does not exist");
			throw new EmployeeDoesnotExist("Employee with Id:-" + leaveRequestDto.getEmployeeId() + " does not exist");
		});
		LeaveRequest leaveRequest = new LeaveRequest();

		leaveRequest.setRequestId("LMS" + String.format("%03d", leaveRequestRepository.count() + 1));
		leaveRequest.setEmployee(employee);
		leaveRequest.setManagerId(employee.getManagerId());
		leaveRequest.setRequestType(RequestType.valueOf(leaveRequestDto.getRequestType()));
		leaveRequest.setRequestStatus(RequestStatus.SUBMITTED);
		BeanUtils.copyProperties(leaveRequestDto, leaveRequest);
		leaveRequestRepository.save(leaveRequest);
		log.info("Leave Request Submitted Successfully");

		Action action = Action.builder().actiontakentype("LMS").actiontaken(leaveRequest.getRequestStatus().toString())
				.employee(employee).actiontakentime(LocalDateTime.now()).requestId(leaveRequest.getRequestId()).build();

		actionRepository.save(action);

		ManagerRequestDto managerRequestDto = ManagerRequestDto.builder()

				.approverId(employee.getManagerId()).employeeId(employee.getEmployeeId())
				.requestId(leaveRequest.getRequestId()).startDateTime(leaveRequest.getStartDateTime())
				.endDateTime(leaveRequest.getEndDateTime()).requestType(leaveRequest.getRequestType().toString())
				.requestStatus(RequestStatus.PENDING.toString()).reason(leaveRequest.getReason()).build();
		kafkaSender.send(managerRequestDto);

		return new ResponseDto("Leave Request Submitted Successfully");

	}

	public LeaveDto getRequest(String request_id) {

		LeaveRequest leaveRequest = leaveRequestRepository.findById(request_id).orElseThrow(() -> {
			throw new NoDataFound("No data found on this request");
		});
		LeaveDto leaveDto = LeaveDto.builder().employeeId(leaveRequest.getEmployee().getEmployeeId())
				.managerId(leaveRequest.getManagerId()).startDateTime(leaveRequest.getStartDateTime())
				.endDateTime(leaveRequest.getEndDateTime()).requestType(leaveRequest.getRequestType().toString())
				.requestStatus(leaveRequest.getRequestStatus().toString()).reason(leaveRequest.getReason())
				.creationDateTime(leaveRequest.getCreationDateTime())
				.updationDateTime(leaveRequest.getUpdationDateTime()).build();

		return leaveDto;

	}

	public ResponseDto deleterequest(String request_id) {
		LeaveRequest leaveRequest = leaveRequestRepository.findByRequestId(request_id).orElseThrow(() -> {
			throw new NoDataFound("No data found on this request");
		});
		if (leaveRequest.getRequestStatus().equals(RequestStatus.CANCELLED)) {
			throw new LeaveRequestAlreadyCancelledException("Leave request already cancelled ");
		}
		leaveRequest.setRequestStatus(RequestStatus.CANCELLED);
		leaveRequest.setUpdationDateTime(LocalDateTime.now());
		leaveRequestRepository.save(leaveRequest);
		Action action = Action.builder().actiontakentype(leaveRequest.getRequestType().toString())
				.actiontaken(leaveRequest.getRequestStatus().toString()).employee(leaveRequest.getEmployee())
				.requestId(leaveRequest.getRequestId()).actiontakentime(LocalDateTime.now()).build();
		actionRepository.save(action);

		ApproverRequest approverRequest = approverRequestRepository.findBySourcerequestId(request_id);
		approverRequest.setStatus(leaveRequest.getRequestStatus().toString());
		approverRequest.setUpdationDateTime(LocalDateTime.now());
		approverRequestRepository.save(approverRequest);
		return new ResponseDto("Leave Request Cancelled successfully");

	}

	public ResponseDto approverupdate(ApproverDto approverDto) {
		Employee employee = employeeRepository.findByEmployeeId(approverDto.getApproverId()).orElseThrow(() -> {

			throw new EmployeeDoesnotExist("Employee with Id:- " + approverDto.getApproverId() + " does not exist");
		});
		

		LeaveRequest leaveRequest = leaveRequestRepository.findById(approverDto.getRequestId()).orElseThrow(() -> {
			throw new NoDataFound("No data found on this request");
		});
		if (!leaveRequest.getManagerId().equals(approverDto.getApproverId()))

		{
			throw new NotAManager("You are not a manager for this request to take the action");
		}
		if (!employee.getLogStatus().equals(LogStatus.LOGIN)) {
			throw new LoginRequiredException("Login Required");
		}

		Action action = Action.builder().actiontakentype(leaveRequest.getRequestType().toString())
				.actiontaken(approverDto.getStatus()).employee(employee).actiontakentime(LocalDateTime.now())
				.requestId(leaveRequest.getRequestId()).build();

		actionRepository.save(action);

		
		leaveRequest.setRequestStatus(RequestStatus.valueOf(approverDto.getStatus()));
		leaveRequest.setRemarks(approverDto.getRemarks());
		leaveRequestRepository.save(leaveRequest);

		ApproverUpdateDto approverUpdateDto = ApproverUpdateDto.builder()
				.remarks(approverDto.getRemarks())
			
				.sourcerequestId(approverDto.getRequestId()).status(approverDto.getStatus())
				.build();

		leavedatasender.send(approverUpdateDto);

		return new ResponseDto("Action taken on the request");
	}

}
