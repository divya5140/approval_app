package com.example.approval.system.kafka.receiver;

import java.util.Optional;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.approval.system.dto.LeaveUpdateDto;
import com.example.approval.system.entity.LeaveRequest;
import com.example.approval.system.entity.RequestStatus;
import com.example.approval.system.repository.LeaveRequestRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class LeaveDataReceiver {

	private final LeaveRequestRepository leaveRequestRepository;


	@KafkaListener(topics = "leaveupdate", groupId = "groups-id", containerFactory = "KafkaListenerContainerFactory")
	public void consume(LeaveUpdateDto leaveUpdateDto) {

		Optional<LeaveRequest> leaveRequest=leaveRequestRepository.findByRequestId(leaveUpdateDto.getSourcerequestId());

		leaveRequest.get().setRequestStatus(RequestStatus.valueOf(leaveUpdateDto.getStatus()));
		leaveRequest.get().setRemarks(leaveUpdateDto.getRemarks());
		leaveRequestRepository.save(leaveRequest.get());
		log.info(leaveUpdateDto.toString());

		
		
	}

}
