package com.example.approval.system.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.approval.system.dto.ApproverRequestDto;
import com.example.approval.system.dto.DashboardDto;
import com.example.approval.system.entity.ApproverRequest;
import com.example.approval.system.entity.Employee;
import com.example.approval.system.exception.EmployeeDoesnotExist;
import com.example.approval.system.repository.ApproverRequestRepository;
import com.example.approval.system.repository.EmployeeRepository;
import com.example.approval.system.service.DashboardService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DashboardServiceImpl implements DashboardService {

	private final ApproverRequestRepository approverRequestRepository;

	private final EmployeeRepository employeeRepository;

	public DashboardDto dashboardview(String employeeid) {
		Employee employee = employeeRepository.findByEmployeeId(employeeid).orElseThrow(() -> {
			throw new EmployeeDoesnotExist("No such employee exists");
		});

		DashboardDto dashboardDto = new DashboardDto();

		List<ApproverRequest> approverRequests = approverRequestRepository.findByEmployee(employee);
		List<ApproverRequestDto> approverRequestDtos = approverRequests.stream().map(am -> {
			ApproverRequestDto approverRequestDto = new ApproverRequestDto();
			approverRequestDto.setApprequestId(am.getApprequestId());
			approverRequestDto.setEmployeeId(am.getEmployee().getEmployeeId());
			approverRequestDto.setApproverId(am.getApproverId());
			approverRequestDto.setRequestId(am.getSourcerequestId());
			approverRequestDto.setStatus(am.getStatus());
			approverRequestDto.setDescription(am.getDescription());

			approverRequestDto.setCreationDateTime(am.getCreationDateTime());
			approverRequestDto.setUpdationDateTime(am.getUpdationDateTime());
			return approverRequestDto;
		}).collect(Collectors.toList());
		dashboardDto.setRequestedleaves(approverRequestDtos);

		List<ApproverRequest> approverRequests1 = approverRequestRepository.findByApproverId(employeeid);
		List<ApproverRequestDto> approverRequestDtos1 = approverRequests1.stream().map(am -> {
			ApproverRequestDto approverRequestDto1 = new ApproverRequestDto();
			approverRequestDto1.setApprequestId(am.getApprequestId());
			approverRequestDto1.setEmployeeId(am.getEmployee().getEmployeeId());
			approverRequestDto1.setApproverId(am.getApproverId());
			approverRequestDto1.setRequestId(am.getSourcerequestId());
			approverRequestDto1.setStatus(am.getStatus());
			approverRequestDto1.setDescription(am.getDescription());
			approverRequestDto1.setCreationDateTime(am.getCreationDateTime());
			approverRequestDto1.setUpdationDateTime(am.getUpdationDateTime());
			return approverRequestDto1;
		}).collect(Collectors.toList());

		dashboardDto.setPendingrequests(approverRequestDtos1);
		return dashboardDto;

	}

}
