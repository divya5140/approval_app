package com.example.approval.system.service;

import com.example.approval.system.dto.ApproverDto;
import com.example.approval.system.dto.LeaveDto;
import com.example.approval.system.dto.LeaveRequestDto;
import com.example.approval.system.dto.ResponseDto;

public interface LeaveRequestService {
	
	ResponseDto leaverequest(LeaveRequestDto leaveRequestDto);

	LeaveDto getRequest(String request_id);
	
	ResponseDto deleterequest(String request_id);
	
	ResponseDto approverupdate(ApproverDto approverDto);
}
