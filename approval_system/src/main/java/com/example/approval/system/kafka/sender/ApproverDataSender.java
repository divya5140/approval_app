package com.example.approval.system.kafka.sender;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.approval.system.dto.LeaveUpdateDto;
import com.example.approval.system.kafka.serializer.LeaveUpdateSerializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ApproverDataSender {

	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(LeaveUpdateDto leaveUpdateDto) {
		
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, LeaveUpdateSerializer.class);

		
		Producer<String, LeaveUpdateDto> kafkaProducer = new KafkaProducer<>(properties, new StringSerializer(),
				new LeaveUpdateSerializer());	
		
		log.info("Message Sent: "+leaveUpdateDto);
		System.out.println();
		ProducerRecord<String, LeaveUpdateDto> record = new ProducerRecord<String, LeaveUpdateDto>("leaveupdate", leaveUpdateDto);
		log.info(""+record);
		System.out.println(kafkaProducer.send(record));
		
 
		
	}
}
