package com.example.approval.system.service.impl;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.example.approval.system.dto.ApproverDto;
import com.example.approval.system.dto.LeaveUpdateDto;
import com.example.approval.system.dto.ResponseDto;
import com.example.approval.system.entity.Action;
import com.example.approval.system.entity.ApproverRequest;
import com.example.approval.system.entity.Employee;
import com.example.approval.system.entity.LeaveRequest;
import com.example.approval.system.entity.LogStatus;
import com.example.approval.system.exception.EmployeeDoesnotExist;
import com.example.approval.system.exception.LoginRequiredException;
import com.example.approval.system.exception.NoDataFound;
import com.example.approval.system.exception.NotAManager;
import com.example.approval.system.kafka.sender.ApproverDataSender;
import com.example.approval.system.repository.ActionRepository;
import com.example.approval.system.repository.ApproverRequestRepository;
import com.example.approval.system.repository.EmployeeRepository;
import com.example.approval.system.repository.LeaveRequestRepository;
import com.example.approval.system.service.ApproverRequestService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ApproverRequestServiceImpl implements ApproverRequestService {

	private final LeaveRequestRepository leaveRequestRepository;

	private final ApproverRequestRepository approverRequestRepository;

	private final ActionRepository actionRepository;

	private final EmployeeRepository employeeRepository;

	private final ApproverDataSender approverDataSender;

	public ResponseDto approverrequests(ApproverDto approverDto) {
		Employee employee = employeeRepository.findByEmployeeId(approverDto.getApproverId()).orElseThrow(() -> {

			throw new EmployeeDoesnotExist("Employee with Id:- " + approverDto.getApproverId() + " does not exist");
		});
		

		LeaveRequest leaveRequest = leaveRequestRepository.findById(approverDto.getRequestId()).orElseThrow(() -> {
			throw new NoDataFound("No data found on this request");
		});
		if (!leaveRequest.getManagerId().equals(approverDto.getApproverId()))

		{
			throw new NotAManager("You are not a manager for this request to take the action");
		}
		if (!employee.getLogStatus().equals(LogStatus.LOGIN)) {
			throw new LoginRequiredException("Login Required");
		}

		Action action = Action.builder().actiontakentype(leaveRequest.getRequestType().toString())
				.actiontaken(approverDto.getStatus()).employee(employee).actiontakentime(LocalDateTime.now())
				.requestId(leaveRequest.getRequestId()).build();

		actionRepository.save(action);

		ApproverRequest approverRequests = approverRequestRepository.findBySourcerequestId(approverDto.getRequestId());
		approverRequests.setStatus(approverDto.getStatus());
		
		approverRequests.setRemarks(approverDto.getRemarks());

		approverRequestRepository.save(approverRequests);

		LeaveUpdateDto leaveUpdateDto = LeaveUpdateDto.builder().sourcerequestId(leaveRequest.getRequestId())
				.status(approverDto.getStatus())
				.remarks(approverDto.getRemarks()).build();
		approverDataSender.send(leaveUpdateDto);

		return new ResponseDto("Action taken on the request");
	}
}